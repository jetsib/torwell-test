#include <stdio.h>
#include <math.h>

#define HS 62
typedef struct{
    int number;
    char *string;
}smap_t;

smap_t smap[]={
    {26346,"MgK"},{26676,"MKd"},{28161,"GVT"},{31622,"vNx"},{31873,"vwP"},{32028,"vV9"},{35260,"zAj"},{36368,"zO9"},{36428,"zu1"},
    {38716,"AcO"},{38805,"APF"},{40111,"AHl"},{40893,"ALW"},{40956,"AQ9"},{40957,"AQs"},{40958,"AQq"},{40959,"AQL"},{40960,"AQQ"},
    {42541,"Zcz"},{47987,"yuk"},{49137,"yh2"},{49169,"yIi"},{49443,"yau"},{49444,"yap"},{49639,"ytL"},{50047,"T7T"},{50048,"T7N"},
    {50127,"Tx8"},{50128,"TxE"},{50129,"Tx2"},{51183,"Tn2"},{51184,"Tn1"},{51204,"Tnf"},{52071,"T2a"},{52977,"Thu"},{54650,"NTO"},
    {55908,"N2e"},{56100,"N9g"},{57924,"ocd"},{60538,"oeH"},{60539,"oe0"},{60540,"oeO"},{61474,"okE"},{63141,"dH6"},{64436,"dUw"},
    {64437,"dUn"},{67416,"m2D"},{72677,"wtT"},{73039,"nCi"},{73040,"nCc"},{73211,"nxJ"},{73654,"nzb"},{73756,"nZq"},{74037,"ndz"},
    {74038,"ndA"},{76081,"nIG"},{76082,"nIv"},{79467,"VXS"},{79564,"VYw"},{79790,"VeK"},{79791,"Vel"},{0,NULL}
};

char maps[HS]={0};

char *function(int number){
    static char tout[4]={0};
    int level=3;
    while (number){
        level--;
        tout[level]=maps[number%HS];
        number/=HS;
    }
    return tout;
}

void main(){
    int i,imp=0,pos=0;
    for(i=0;smap[i].number>0;i++){
        int tnumber=smap[i].number;
        int eqc,level=3;
        while (tnumber){
            level--;
            eqc=tnumber%HS;
            tnumber/=HS;
            if (!maps[eqc]){
                pos++;
                maps[eqc]=smap[i].string[level];
                //printf("MAP(%d %c)\n",eqc,smap[i].string[level]);
            }else if (maps[eqc]==smap[i].string[level]){
                //printf("FACT(%d %s %d %c)\n",smap[i].number,smap[i].string,eqc,smap[i].string[level]);
            }else if (maps[eqc]!=smap[i].string[level]){
                printf("impossible(%d %s %d %c)\n",smap[i].number,smap[i].string,eqc,smap[i].string[level]);
                imp++;
            }
        }
    }
    if (!imp && pos==HS){
        printf("MAP(%.*s",HS,maps);
    }else{
        printf("POS %d  IMP %d SURVEY %d\n",pos,imp,i);
        for (i=0;i<HS;i++){
            if(maps[i])
                printf("%2d=>%c",i,maps[i]);
            else
                printf("%d \\0",i);
            if ((i+1)%6==0) printf("\n"); else if ((i+1)<HS) printf(","); else printf("\n");
        }
    }

    ///test
    int fault=0;
    for(i=0;smap[i].number>0;i++){
        if (strcmp(function(smap[i].number),smap[i].string)==0)
            printf("O");
        else{
            printf("WRONG MATCH:%d Orig:%s Calc:%s\n",smap[i].number,smap[i].string,function(smap[i].number));
            fault++;
        }
    }

    if (!fault)
        printf("\nSOLUTION IS OK...\n");
    else
        printf("\nSOLUTION IS WRONG!!!\n");

    printf("f(%d)=%s\n",30001,function(30001));
    printf("f(%d)=%s\n",55555,function(55555));
    printf("f(%d)=%s\n",77788,function(77788));
    
    printf("f(%d)=%s\n",pow(HS,3)-1,function(pow(HS,3)-1));

}
